# Overview
Visão geral da ferramenta

## Tela Inicial
![](./Imagens/image_2.png)
=======
![](./imagens/image_2.png)

1. Contém os Itens: Ask a question, Browse Data, Criar Dash/Pulses, SQL, Configuração.
2. Barra de pesquisa
3. Home page
4. Bem vindo Fulano
5. Suas Collections
6. Seus Datasets

## Dicionário de Termos 

**MBQL:** Metabase Query Language, é a linguagem usada para expressar consultas "estruturadas" na Metabase. Ele é gerado pelo Query Builder no front-end da Metabase e traduzido para uma infinidade de linguagens de consulta "nativas" - incluindo SQL, Pipelines de agregação Mongo e Druid - pelo back-end. (A metabase também pode executar consultas nessas linguagens nativas diretamente, mas essas não são consideradas parte do MBQL).

**Question:** às vezes chamada de "card", inclui uma consulta (dataset_query que é uma consulta estruturada ou consulta nativa), além de informações sobre como deve ser visualizada (display e visualization_settings) e quaisquer parâmetros expostos.  

**Dashboards:** uma coleção de perguntas organizadas em uma grade. Composto por DashCards que podem combinar uma ou mais perguntas de um tipo semelhante em uma série. Também pode expor parâmetros e mapear esses parâmetros para as perguntas no painel.  

**Pulse:** um conjunto de perguntas, juntamente com um agendamento e e-mail ou destinatário do Slack, que serão executados e enviados nesse agendamento.  

**Collections:** uma coleção de perguntas, painéis, pulsos e outras coleções. Funciona como uma pasta para organização.  

**Alert:** semelhante a um pulso, mas associado a uma única pergunta.  

**Database:** um banco de dados, data warehouse ou outra fonte de dados (por exemplo, Google Analytics) que pode ser consultada na Metabase. Tem muitas tabelas.

**Table:** uma tabela (ou outra unidade da organização, coleção e.x. MongoDB, site do Google Analytics) no banco de dados. Tem muitos campos.  

**Field:** uma coluna (ou um atributo de um registro) em uma Tabela.  

**Column:** uma coluna em um conjunto de dados de resultados.  

**Dimension:** uma referência a um campo (possivelmente via chave estrangeira) ou expressão personalizada em uma consulta estruturada e, em alguns casos, uma referência a uma coluna de agregação na mesma consulta. Além disso, os campos numéricos e de data / hora podem ser binados.  
* Referidas como referências concretas, de expressão e agregação no MBQL.

**Structured Query:** às vezes chamada de "consulta GUI", uma definição de consulta MBQL.  Native Query: às vezes chamada de "consulta SQL", uma consulta escrita na linguagem de consulta "nativa" do banco de dados, e.x. SQL

**Aggregation:** às vezes chamada de "métrica", uma agregação MBQL em uma Consulta Estruturada, e.x. contagem, soma, média, etc.  

**Breakout:** essencialmente uma cláusula GROUP BY, às vezes chamada de "dimensão", uma dimensão MBQL em uma consulta estruturada.  

**Filter:**  um filtro MBQL em uma Consulta Estruturada.

**Parameter:** uma variável que pode ser alterada pelo usuário de uma pergunta ou painel por meio de um widget de parâmetro ou da string de consulta da URL. Um parâmetro pode ser mapeado para uma dimensão em uma consulta estruturada ou uma tag de modelo em uma consulta nativa. 

**Template Tag:** um espaço reservado em uma consulta nativa (atualmente apenas SQL) que pode ser preenchido com um valor primitivo (sequência, número ou data) ou um filtro de campo por meio de um parâmetro.  

**Segment:** um conjunto salvo de filtros em uma tabela que pode ser aplicado a uma consulta estruturada.  

**Metric:** uma agregação salva e um conjunto opcional de filtros em uma tabela que pode ser adicionada como uma agregação a uma consulta estruturada.

