# Dashboards
Podemos dizer que um dashboard é um painel visual que apresenta, de maneira centralizada, um conjunto informações como por exemplo: indicadores(dimensões) e métricas.

## Criação de Dashboard

Agora hora de montarmos uma visualização com as questões que criamos.  
Para isso vá no sinal de + no canto superior direito.  
Selecione New Dashboard  

![](./Imagens/image_32.png)

Salve o Dashboard com o nome de sua preferência, lembre-se de escolher a sua collection pessoal.  

![](./Imagens/image_33.png)

Será exibida a tela abaixo:  

![](./Imagens/image_34.png)

O Que significa cada item?
1. Nome e collection do Dashboard; 
2. Inserção de question; 
3. Modo de edição do Dashboard; 
4. Mover o Dashboard de collection; 
5. Copiar o Dashboard; 
6. Tempo de refresh automático; 
7. Visualizar em tela cheia;

Clique em Edit Dashboard. Você terá acesso a essa tela.
![](./Imagens/image_35.png)

Ao canto esquerdo você pode editar o nome e a descrição do Dash.  

A direita você tem as seguintes opções ordenadas da esquerda para a direita:  
* Inserir question
* Inserir Filtro
* Inserir caixa de texto
* Revision History (aqui você ve todo o histórico de edição do dash)

Acima a direita você verá o botão de save.  

**OBS.: Lembre-se sempre de salvar o Dash antes de apertar um F5, afinal você não quer perder o trabalho que esta sendo feito né...**


## Adicionando uma Question
Clique em Add a Question, navegue pelos caminhos de sua coleção pessoal > Treinamento.  


![](./Imagens/image_36.png)

Note que você verá todas as questions criadas, caso tenha salvo elas em outra collection, jogue elas na collection treinamento dentro de sua collection pessoal.  

Após isso selecione as três questions marcadas na imagem ao lado. Depois salve o Dashboard.  
![](./Imagens/image_37.png)

Para movimentar os cards dentro do Dashboard, você pode arrastar eles e redimensionar.   
Para redimensionar basta passar o mouse sobre o canto inferior direito quando aparecer o ícone de re-dimensionar clique segure e arraste o ícone que possui duas setas.   
![](./Imagens/image_38.png)

Note que o Metabase tem uns quadriculados ao fundo, esse quadriculado é onde ele controla o grid para o tamanho dos itens.  
Alguns itens tem um tamanho mínimo para ficar no dashboard (infelizmente para Markdown a largura é 4 e a altura é 1, isso faz com que fique ruim re-dimensionar imagens).  

## Inserção de Caixa de Texto


Insira uma caixa de texto para podermos colocar um título e campos para link entre páginas. Arraste os campos para ficarem da forma abaixo.  

![](./Imagens/image_39.png)

Ao passar o mouse sobre o card, aparecerá um ícone com uma engrenagem, clique nele, entrará em uma janela de configuração. Faça isso para o card de total de pedidos e troque o nome.  

![](imagens/image_40.png)

## Modificação de texto

Ao passar o mouse sobre o card(da caixa de texto), aparecerá um ícone com uma engrenagem, clique nele, entrará em uma janela de configuração como ao lado.  
Centralize os alinhamentos verticais e horizontais. (Middle e Center)  

Escreva # KPI’s e nas linhas de Baixo:
```
## Visão Geral
## Vendas
```
![](./Imagens/image_41.png)

Deixe os cards do Dashboard conforme abaixo:  

![](./Imagens/image_42.png)

## Adicionando um Filtro
Clique em Add a Filter para adicionar um filtro e selecione Location FIlter. No Campo dos cards é possível selecionarmos as colunas que queremos filtrar. Selecione State  

![](./Imagens/image_43.png)

Daremos uma pausa para entender a linguagem de texto Markdown.  

--- 

## Markdown

Markdown é uma linguagem simples de conversão text-to-HTML. Algumas das plataformas a usam para facilitar a escrita de documentação. alguns exemplos são os artigos do Linkedin, Github, Jupyter Notebooks, Medium, etc…

As vantagens é que além de ser simples em alguns sites você pode usar a sintaxe de HTMl sem problemas que ele entende.   
Obs.: O Metabase até a versão 0.34.1 não aceita sintaxe HTML para markdown. As outras limitações irei colocando conforme for progredindo no material.  

## TITULOS

Titulos para títulos de H1 a H6 usamos o #:  
# Título nível 1
## Título nível 2
### Título nível 3
#### Título nível 4
##### Título nível 5
###### Título nível 6

## Paragráfos, Hiperlinks e Imagens

Para escrever um parágrafo basta escrever normalmente.
Para deixar um texto em negrito escreva uma palavra usando duplo asteriscot ( ** ) antes e depois da palavra por exemplo:  
```**negrito**```  
Para italico ```*italico*```x.  

Para usar hiperlink:  
```[texto](www.google.com.br)```  

Para linkar uma imagem:  
```![texto](link da imagem)```

## Listas

Para criar listas com pontos  use o * seguido de um espaço.  

```
* item 1
* item 2
```
Para criar listas com números, use o número seguido de um . seguido de um espaço:  
```
1. item1
2. item2
```
--- 
## De Volta aos Dashboards

De volta aos dashboards, vamos criar a segunda página e melhorar nossa análise. A primeira etapa é criar um novo Dashboard com o nome de Treinamento Vendas.  

Após isso pegue o link do Dashboard no navegador. Será algo como https://Dominio/dashboard/número

![](./Imagens/image_44.png)

Vamos colar este endereço no nosso primeiro Dashboard.   
Na caixa de texto troque onde está escrito Vendas para [Vendas](Link)  

![](./Imagens/image_45.png)

Após isso clique no olho ao canto esquerdo para ver como fica a prévia do campo com o link.  

![](./Imagens/image_46.png)

Entre no Dashboard Treinamento Vendas e adicione nele os seguintes cards:  

![](./Imagens/image_47.png)

Crie uma caixa de texto e coloque no topo para poder fazer um cabeçalho como no Dashboard de visão geral, mas agora coloque o link do Dashboard de visão geral, e não deixe o link no dashboard de Vendas.  

![](./Imagens/image_48.png)

Agrupe desta forma os Cards, e adicione as caixas de texto conforme o layout, adicione no tamanho que desejar.

![](./Imagens/image_49.png)

O mesmo para os outros itens.

![](./Imagens/image_50.png)


Temos várias questions, do Dashboard de Sample, foi criado vários um para cada ano(Série temporal de receita/vendas por dia). 
Vamos estar adicionando eles no nosso de 2016 para que fiquem sobrepostos. Passe o mouse sobre  a question, ao ver o símbolo Add+ aparecer clique nele.  
![](./Imagens/image_51.png)


Agora marque os anos 2017, 2018, 2019. e clique em Done.

![](./Imagens/image_52.png)

**Saia e Salve o Dashboard. Faça um teste clicando nos links e alterando entre eles.**

![](./Imagens/image_53.png)
![](./Imagens/image_54.png)

## EXTRA TASK

Mas será que o Design de nossa tela de visão geral realmente está respondendo todas as perguntas necessárias?  

Para ser sincero está horrível.

**Vamos Gerar mais questions.** 


Vamos colocar os Trends de :
* Receita líquida (Receita total - impostos - Descontos)
* Total de descontos.
* Quantidade de pedidos(quantity)
* Quantidade de notas 4 e 5 nos Reviews.
* Fazer em trends mensal.
* Editar a receita total, e os clientes cadastrados os transformando em trends.

Como exemplo, tente gerar algo como o da imagem, mas fique a vontade para liberar sua criatividade.

![](./Imagens/image_55.png)

Os cards estão um pouco melhor do que antes. Mas claro que ainda dá para melhorar.  
![](./Imagens/image_56.png)

Não se esqueça de trocar os labels dos gráficos.  
Isso torna mais fácil a leitura.  
Fique a vontade para adicionar quantos cards a mais desejar.  
Dica: Tente buscar formatações de texto e números de forma que fique fácil o entendimento, caso ainda possa parecer confuso adicione alguns textos com markown para que o fique entendível as informações.  

![](./Imagens/image_57.png)