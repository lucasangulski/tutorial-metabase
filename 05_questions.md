# Questions
Um pouquinho sobre questions

## Bora lá 

Existem três tipos de questões no Metabase:

1. O modo de pergunta simples, que permite filtrar, resumir e visualizar dados.  
2. O modo de perguntas personalizadas, que fornece um poderoso editor no estilo de notebook para criar perguntas mais complexas que exigem junções, vários estágios de filtragem e agregação ou colunas personalizadas.  
3. O editor de consultas SQL / nativo.  

Obs.: Sempre que formos criar uma Question precisamos clicar em Ask a Question, para os passos futuros omitirei essa informação.  

Veremos os dois primeiros nesta primeira etapa, posteriormente teremos um outro material para abranger de forma mais profunda SQL e a forma que o Metabase trata o SQL.  

## Simple Questions

Primeiro clique em Ask a Question  

![](./Imagens/image_12.png)

Clique em Simple Question

![](./Imagens/image_13.png)

Será mostrada a janela de adicionar o dado, selecione Sample Dataset.

![](./Imagens/image_14.png)

Depois selecione a tabela Orders. 
![](./Imagens/image_15.png)

Agora será mostrado essa janela com todos os dados da tabela

![](./Imagens/image_16.png)

Ao canto direito no campo Filter teremos acesso as colunas da tabela.
Note que terá acesso a outras tabelas também , isso só ocorre devido a estas tabelas terem Chaves primárias e estrangeiras, vinda do banco de dados.
O Metabase Mapeia isso. 

![](./Imagens/image_17.png)

Em Summarize, temos as métricas e dimensões que podemos estar agregando os dados.
![](./Imagens/image_18.png)

Dentro de Summarize, vamos agregar os dados pegando a receita total (soma da coluna Total).

![](./Imagens/image_19.png)

Se clicar em visualization verá que teremos acesso a outros tipos de gráficos.
Após selecionar o tipo clique em Done. 

![](./Imagens/image_20.png)

Depois clique em settings na visualização e você verá estes campos:
* **Style:** Estilo da apresentação do dado
* **Separator Style:** Tipo de separação “,” ou “.”
* **Minimum number of decimal places:** Número de casas decimais.
* **Multiply by a number:** Multiplica o dado por um valor
* **Add a prefix:** Adiciona um Prefixo
* **Add a suffix:** Adiciona um Sufixo

Ao clicar em *Save a question*, você terá acesso a tela mostrada na imagem:  

Em **Name** digite um nome fácil de encontrar.  
Em **Description** digite uma descrição.  
Em **Collection** para armazenar, selecione sua collection pessoal.  

Obs. O Metabase permite ter questions com mesmo nome.

![](./Imagens/image_21.png)


Após salvar, aparecerá uma tela perguntando se você deseja inserir esta question em um Dashboard.  

Por enquanto clique em Not Now.
![](./Imagens/image_22.png)
Agora arraste sua question para a collection treinamento.
![](./Imagens/image_23.png)

Obs.: Em alguns casos não consegui salvar automaticamente na collection treinamento dentro da collection pessoal (talvez por ser um bug na versão utilizada). Neste caso contornei a situação movendo todos os questions e dashboards depois de criados. (provavelmente por causa da versão usada na época - 2019)

### Hints

Dicas: Quando for criar perguntas  pense em qual a pergunta atrelada ao negócio ou problema que você quer responder.  
Embora o nome seja question, ela trará a resposta.  
Crie uma hierarquia de collections de acordo com sua necessidade e com um bom entendimento para separar bem seus cards e dashboards.  
Teste sempre as funcionalidades com coisas básicas para depois realizar a versão final.  

### Exercício Simple Questions

**Vamos responder mais algumas perguntas:**
1. Qual a quantidade de pedidos ao longo do tempo?  
2. Qual o total de produtos cadastrados?  
3. Quantos clientes estão cadastrados?   
4. Quantos clientes temos por estado?  
5. Qual a maior fonte de entrada destes clientes?(de onde vieram? Twitter, facebook, etc)  

---

## Custom Questions

Agora vamos criar uma questão personalizável. 
Aqui a grande vantagem é poder usar Joins, selecionar a quantidade de linhas, criar métricas por equação.  

![](./Imagens/image_24.png)

Clique em Ask a Question, Selecione Custom Question, Selecione Sample Dataset, selecione a tabela People.

![](./Imagens/image_25.png)

*Nesta Tela você terá acesso a criar colunas, Joins, Filtros, Agregação, ordenar, limitar, e visualizar os dados de acordo com os parâmetros selecionados.*  

Clique em Join, selecione a tabela ORDERS, Em Campos de join, selecione ID e User_ID, o primeiro campos é referente a primeira tabela, e o Segundo ao campo de chave de conexão da Segunda tabela.

![](./Imagens/image_26.png)

*Após isso clique no símbolo de Play ao lado direito da linha de onde foi feito o Join, para ver a prévia dos dados, você verá uma tabela conforme a que esta na imagem.* 

### Como as Tabelas estão linkadas?
A esquerda temos o ERD (Entity Relationshio Diagram) do dataset Sample.  
Isso nos dá até uma visão geral de como podemos fazer as perguntas ao Metabase.  

![](./Imagens/image_27.png)


### Join

Até a versão usada para criar o material o metabase só possui os seguintes joins.  

1. **Inner Join:** Ele faz a interseção entre as duas tabelas, ou seja pega o campo comum em uma e em outra, tirando do resultado o que não é igual
2. **Left Join:** Ele traz tudo que está do lado esquerdo do JOIN ou seja a primeira tabela, tanto os registros que não se relacionam com a segunda tabela, quanto os que fazem parte da interseção com a segunda tabela.
3. **Right Join:** Ele traz tudo que está do lado esquerdo do JOIN ou seja a segunda tabela, tanto os registros que não se relacionam com a primeira tabela, quanto os que fazem parte da interseção com a primeira tabela.

![](./Imagens/image_28.png)

### Bora criar mais questions

Selecione em Summarize States da tabela PEOPLE para agrupamento.  

Para o valor total de receita selecione SUM of TOTAL da tabela ORDERS.  

![](./Imagens/image_29.png)

Fique livre para fazer as modificações que desejar.  

![](./Imagens/image_30.png)

Salve como Receita por estado em sua collection pessoal.  

![](./Imagens/image_31.png)

### Exercícios 

1. Qual as 5 categorias mais compradas no estado de Los angeles?
2. Qual os cinco pedidos mais vendidos?
3. Qual o usuário que comprou mais produtos?
4. Quais os 10 produtos que tiveram maior índice de avaliação?
5. Qual o total de impostos pagos em referência ao ano anterior?
