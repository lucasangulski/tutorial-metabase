# Refências

## Documentação do Metabase

* [Collections](https://www.metabase.com/docs/latest/administration-guide/06-collections.html)
* [Simple Questions](https://www.metabase.com/docs/v0.34.1/users-guide/04-asking-questions.html)
* [Custom Questions](https://www.metabase.com/docs/v0.34.1/users-guide/custom-questions.html)
* [Dashboards](https://www.metabase.com/docs/v0.34.1/users-guide/07-dashboards.html)
* [Filtros de Dashboards](https://www.metabase.com/docs/v0.34.1/users-guide/08-dashboard-filters.html)
* [Pulses](https://www.metabase.com/docs/v0.34.1/users-guide/10-pulses.html)
* [Guia do usuário](https://www.metabase.com/docs/v0.34.1/users-guide/start.html)
* [Tipos de Visualização de resultados](https://www.metabase.com/docs/v0.34.1/users-guide/05-visualizing-results.html)
* [Public Links](https://www.metabase.com/docs/latest/administration-guide/12-public-links.html)

## Geral 

* [Joins](http://bidescomplicado.blogspot.com/2013/10/os-tipos-de-join-quais-sao-e-como-usa.html)
* [Markdown Guide](https://www.markdownguide.org/)
* [Aprenda Markdown](https://blog.da2k.com.br/2015/02/08/aprenda-markdown/)
* [Storytelling com dados](https://www.amazon.com.br/Storytelling-com-Dados-Visualiza%C3%A7%C3%A3o-Profissionais/dp/8550804681/ref=sr_1_1?adgrpid=82022319300&gclid=EAIaIQobChMIu5e_iaPv5wIVggWRCh1RbwzREAAYASAAEgJ0nvD_BwE&hvadid=392902894935&hvdev=c&hvlocphy=1001773&hvnetw=g&hvqmt=e&hvrand=1480516077027610154&hvtargid=kwd-570348077650&hydadcr=5653_10696953&keywords=storytelling+com+dados&qid=1582722046&sr=8-1)
* [Storytelling com dados: Let’s Practice!](https://www.amazon.com.br/Storytelling-Data-Cole-Nussbaumer-Knaflic/dp/1119621496/ref=sr_1_5?adgrpid=82022319300&gclid=EAIaIQobChMIu5e_iaPv5wIVggWRCh1RbwzREAAYASAAEgJ0nvD_BwE&hvadid=392902894935&hvdev=c&hvlocphy=1001773&hvnetw=g&hvqmt=e&hvrand=1480516077027610154&hvtargid=kwd-570348077650&hydadcr=5653_10696953&keywords=storytelling+com+dados&qid=1582722046&sr=8-5)
* [Blog storytellingwithdata](http://www.storytellingwithdata.com/)
