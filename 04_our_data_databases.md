# Our Data - Databases
Acesso e visualização as bases para dicionário de dados e entendimentos.

## Aprendendo mais sobre os dados

Na tela inicial, temos uma região chamada Our Data, que lista todos do databases que temos acesso no Metabase. 

Esse campo é interessante, pois nele podemos criar um dicionário sobre estes dados.

![](./Imagens/image_6.png)

Ao acessar você terá acesso as informações e detalhes sobre os dados.  

![](./Imagens/image_7.png)


Ao clicar em tables in this dataset, você terá acesso aos detalhes, possiveis, questões (potentially usefull questions), campos da tabela, questões criadas e Raio-x da tabela.

![](./Imagens/image_8.png)

Em Fields of this table, você tem acesso às informações de cada coluna, como o tipo dela no dataset, e sua descrição.


![](./Imagens/image_9.png)

Em Questions about this table, você terá acesso às questões que foram criadas sobre este dataset.

![](./Imagens/image_10.png)

O Recurso X-Ray é muito interessante, principalmente se você está começando a aprender e entender sobre o dataset em questão.   

Ou até mesmo em buscar enxergar alguma resposta que você não percebeu sobre o dado.

![](./Imagens/image_11.png)