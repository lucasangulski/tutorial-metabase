# Collections
O que são e como criar collections

## O que são
As collections são importantes para podermos organizar a estrutura de nossos cards, isso facilitar saber onde estão e diferenciar eles de acordo com o tipo de relatório da ser gerado.

## Para criar 

**Clique em browse all items** 

![Scheme](./Imagens/image_3.png)

**Clique em New Collection**
![](./Imagens/image_4.png)

Coloque o nome como Treinamento. 
Em descrição coloque a descrição que desejar.
Em collection a ser salva selecione a sua collection pessoal. 




**Clique em Create**
![](./Imagens/image_5.png)

Ao clicar em sua collection, você verá a sua collection Criada.