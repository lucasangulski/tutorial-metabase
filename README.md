# Introdução ao Metabase

## Objetivos


* Iniciar o processo de democratização dos dados.
* Aprender o básico do Metabase.
* Não criar somente relatórios, mas também questionar mais sobre  o que o dado tem a oferecer. 
* Gerar mais valor através das informações que os dados mostram.
* Não mostrar somente a ferramenta mas também como podemos aprender mais com os dados. 

Note que embora o Material vá ensinar o básico da ferramenta é importante que você questione o que mais posso responder de perguntas. 

Explore a ferramenta, e mais ainda explore os dados.

## Como foi pensado o material?

Recomendo usar de acordo com a sua necessidade, podendo ir direto ao ponto de interesse ou seguindo de forma linear.   
Você ainda pode seguir os exemplos usados ou se preferir ler e aplicar ao seu contexto. 

O material foir criado de forma mais genérica possível para lhe ajudar no seu contexto.  

Obs: Recomendo utilizar o método que achar mais fácil de instalar.

**Versão utilizada para criação do material : 0.34** 

## Material feito para a comunidade

## Indice

1. [Sobre o Metabase](01_sobre_o_metabase.md)
2. [Overview](02_overview.md)
3. [Collections](03_collections.md)
4. [Our Data - Databases](04_our_data_databases.md)
5. [Questions](05_questions.md)
6. [Dashboards](06_dashboards.md)
7. [Pulses](07_pulses.md)
8. [Exportação e Compartilhamentos](08_ecportacao_e_compartilhamento.md)
9. [Concluindo](09_concluindo.md)
10. [Referências](10_referencias.md)

## Referências

* https://www.metabase.com/docs/latest/getting-started.html
* https://www.metabase.com/docs/latest/operations-guide/installing-metabase.html
