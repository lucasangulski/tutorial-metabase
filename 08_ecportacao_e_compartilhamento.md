# Exportação e Compartilhamento (Admin)
Vamos aprender como compartilhar e exportar o nosso trabalho.  

## Bora lá
A exportação permite que seja possível compartilhar os dashboards através de links públicos, isso permite que os dashboards fique visível para aqueles com estes links.  

Obs.: Somente o usuário admin pode dar a permissão para compartilhamento dos dashboards.

Esta é a tela do painel de admin, onde deve ser ativado esta opção:


O caminho é:  
Admin> Settings > Public Sharing  
Marque Enable  

![](./Imagens/image_62.png)


Para estar compartilhando vá no dashboard de sua escolha. clique em sharing and embeding.  
![](./Imagens/image_63.png)

Na tela que abrir marque Enable Sharing.  
Após isso basta pegar o link e compartilhar.  

![](./Imagens/image_64.png)




