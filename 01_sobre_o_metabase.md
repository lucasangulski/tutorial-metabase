# Sobre o Metabase
O Metabase é uma ferramenta open source, isto é de código aberto que permite realizar perguntas e aprender com os dados.


![](./Imagens/image_0.png)
## Por Quê Metabase?

* Grátis
* Possui integração/conexão com diversos bancos
* É Fácil de usar
* Usa a engine da origem para realizar as queries
* Diversas empresas o usam algumas brasileiras como: NUBANK, HOTMART, [ETC…​](https://www.metabase.com/case_studies/)
* Simples para democratizar os dados

![](./Imagens/image_1.png)
A democratização de dados tem o intuito de fazer com que todos os funcionários de uma empresa possam saber lidar com os dados que a empresa oferece. Alguns dos pontos que isso traz são:

* Melhoria na tomada de decisão
* Aumento de capacidade técnica dos colaboradores
* Otimização das tecnologias utilizadas pela empresa
* Transparência no compartilhamento de informações

Nesse [artigo](https://medium.com/dataengineerbr/ser-data-driven-faz-sua-empresa-funcionar-devagar-c63895937a60) do Carlos Alberto Cardoso, ele explica em pontos muito relevantes o quanto é custoso tornar a empresa data driven, mas de alguma forma o processo tem de iniciar né.

