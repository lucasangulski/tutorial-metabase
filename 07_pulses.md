# Pulses
Os pulses permitem usar as question para o envio de email automático.

## Criando um Pulse

Para isso vamos criar uma question de Receita agrupada por dia e vamos usar o trend para que ele envie comparando o de hoje com o de ontem.  

![](./Imagens/image_58.png)

Salve a question como Receita por dia - Pulse.  
Após isso clique em Create Pulse.  
Selecione seu collection pessoal.  

![](./Imagens/image_59.png)

Selecione a question  Receita por dia - Pulse.

![](./Imagens/image_60.png)

Adicione seu e-mail e clique em send E-mail now para testar. Caso queira criar o pulse clique em Create.  

![](./Imagens/image_61.png)


--- 
## Dicas

Recomento ir testando e criando mais pulses para familiarizar-se com o recurso.   

Crie os pulses de acordo com as métricas que deseja acompanhar para não ficar muito poluído sua caixa de entrada.  

Dê preferência para Big Numbers ao invés de gráficos pois podem não ser muito legais para ver no email ou slack.  